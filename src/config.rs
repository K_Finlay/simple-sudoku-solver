use std::fs;

use miniserde::{json, Deserialize, Serialize};

#[derive(Default, Serialize, Deserialize)]
pub struct Config {
    pub dark_mode: bool,
}

impl Config {
    pub fn load() -> Self {
        let path = dirs::config_dir().unwrap().join("simple-sudoku-solver/config.json");

        if path.exists() {
            let json_string = fs::read_to_string(path).unwrap();
            json::from_str(&json_string).unwrap()
        } else {
            <Self as Default>::default()
        }
    }

    pub fn save(&self) {
        let path = dirs::config_dir().unwrap().join("simple-sudoku-solver");
        let json_string = json::to_string(&self);

        fs::create_dir_all(&path).unwrap();
        fs::write(path.join("config.json"), json_string).unwrap();
    }
}
