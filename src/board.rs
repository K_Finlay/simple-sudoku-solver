use simple_sudoku_solver_engine::{Col, Row, Solver};
use crate::tile::sudoku_tile;

struct GridConfig {
    visuals: egui::style::WidgetVisuals,
    rect:    egui::Rect,
    radius:  egui::Rounding,

    stroke_regular: egui::Stroke,
    stroke_thin:    egui::Stroke,
}

impl GridConfig {
    fn new(ui: &egui::Ui, rect: &egui::Rect) -> Self {
        let visuals = ui.style().noninteractive();

        Self {
            visuals: *visuals,
            rect:    rect.expand(visuals.expansion),
            radius:  egui::Rounding::from(4.0),

            stroke_regular: egui::Stroke {
                width: 2.0,
                ..visuals.bg_stroke
            },

            stroke_thin: egui::Stroke {
                width: 0.5,
                ..visuals.bg_stroke
            },
        }
    }
}

pub fn sudoku_board<'a>(
    ctx: &'a egui::Context,
    solver: &'a mut Solver,
    error_msg: &'a mut String,
) -> impl egui::Widget + 'a {
    move |ui: &mut egui::Ui| render_board(ui, ctx, solver, error_msg)
}

fn render_board(ui: &mut egui::Ui, ctx: &egui::Context, solver: &mut Solver, error_msg: &mut String) -> egui::Response {
    let desired_size = egui::vec2(500.0, 500.0);
    let (rect, response) = ui.allocate_exact_size(desired_size, egui::Sense::hover());

    if !ui.is_rect_visible(rect) {
        return response;
    }

    let config = GridConfig::new(ui, &rect);

    render_bounding_box(&config, ui);
    render_grid_lines(&config, ui);
    render_buttons(&config, ui, ctx, solver, error_msg);

    response
}

/// Draws the bounding box surrounding the Sudoku board.
#[inline]
fn render_bounding_box(config: &GridConfig, ui: &egui::Ui) {
    ui.painter().rect(
        config.rect,
        config.radius,
        config.visuals.bg_fill,
        config.stroke_regular,
    );
}

/// Draws the internal grid lines of the Sudoku board.
#[inline]
fn render_grid_lines(config: &GridConfig, ui: &egui::Ui) {
    let rect = &config.rect;
    let stroke_regular = &config.stroke_regular;
    let stroke_thin = &config.stroke_thin;

    let v_size = rect.width() / 9.0;
    let h_size = rect.height() / 9.0;

    // Draw horizontal lines
    for cell in 1..9 {
        let offset = h_size.mul_add(cell as f32, rect.top());
        let stroke = if cell % 3 == 0 { stroke_regular } else { stroke_thin };

        ui.painter().hline(rect.left()..=rect.right(), offset, *stroke);
    }

    // Draw vertical lines
    for cell in 1..9 {
        let offset = v_size.mul_add(cell as f32, rect.left());
        let stroke = if cell % 3 == 0 { stroke_regular } else { stroke_thin };

        ui.painter().vline(offset, rect.top()..=rect.bottom(), *stroke);
    }
}

#[inline]
fn render_buttons(
    config: &GridConfig,
    ui: &mut egui::Ui,
    ctx: &egui::Context,
    solver: &mut Solver,
    error_msg: &mut String,
) {
    let v_size = config.rect.width() / 9.0;
    let h_size = config.rect.height() / 9.0;

    let v_centre = v_size * 0.5;
    let h_centre = h_size * 0.5;

    for row in 1..10 {
        for col in 1..10 {
            // Calculate the x and y coordinate of the current tile.
            let x = v_size.mul_add(row as f32, config.rect.left()) - v_centre;
            let y = h_size.mul_add(col as f32, config.rect.top()) - h_centre;

            let rect = egui::Rect::from_center_size(egui::pos2(x, y), egui::vec2(45.0, 45.0));

            // Create a new UI region at the given coordinates and render the tile.
            ui.allocate_ui_at_rect(rect, |ui| {
                ui.centered_and_justified(|ui| ui.add(sudoku_tile(ctx, solver, Row(row), Col(col), error_msg)));
            });

            ui.allocate_ui_at_rect(rect, |ui| {
                let tile_val = solver.board[row as usize - 1][col as usize - 1]
                    .current_value
                    .map(|v| v.to_string())
                    .unwrap_or_default();
                ui.centered_and_justified(|ui| ui.heading(tile_val));
            });
        }
    }
}
