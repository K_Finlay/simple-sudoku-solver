mod app;
mod board;
mod config;
mod resources;
mod tile;

use crate::{app::MyApp, board::sudoku_board, config::Config};

fn main() {
    let options = eframe::NativeOptions {
        drag_and_drop_support: false,
        resizable: false,
        ..Default::default()
    };

    eframe::run_native(
        "Simple Sudoku Solver",
        options,
        Box::new(|cc| {
            let app = Box::new(MyApp::default());

            if app.config.dark_mode {
                cc.egui_ctx.set_visuals(egui::style::Visuals::dark());
            } else {
                cc.egui_ctx.set_visuals(egui::style::Visuals::light());
            }

            app
        }),
    );
}
