use egui::Color32;
use simple_sudoku_solver_engine::{Col, Row, Solver};

pub fn sudoku_tile<'a>(
    ctx: &'a egui::Context,
    solver: &'a mut Solver,
    row: Row,
    col: Col,
    error_msg: &'a mut String,
) -> impl egui::Widget + 'a {
    move |ui: &mut egui::Ui| render_tile(ui, ctx, solver, row, col, error_msg)
}

pub fn render_tile(
    ui: &mut egui::Ui,
    ctx: &egui::Context,
    solver: &mut Solver,
    row: Row,
    col: Col,
    error_msg: &mut String,
) -> egui::Response {
    let desired_size = ui.available_size();
    let (rect, response) = ui.allocate_exact_size(desired_size, egui::Sense::click());

    if !ui.is_rect_visible(rect) {
        return response;
    }

    let bg_colour = if ui.style().visuals.dark_mode {
        ui.style().visuals.code_bg_color
    } else {
        Color32::from_gray(200)
    };

    let visuals = ui.style().interact(&response);
    let rect = rect.expand(visuals.expansion);
    let radius = egui::Rounding::from(4.0);
    let stroke = egui::Stroke {
        width: 2.0,
        ..visuals.bg_stroke
    };

    if response.clicked_by(egui::PointerButton::Primary) {
        response.request_focus();
    }

    if response.clicked_by(egui::PointerButton::Secondary) {
        response.surrender_focus();
        if let Err(e) = solver.remove_value_from_board(row, col) {
            *error_msg = e.to_string();
        }
    }

    if response.gained_focus() {
        error_msg.clear();
    }

    if response.has_focus() && response.hovered() {
        ui.painter().rect(rect, radius, bg_colour, stroke);
    } else if response.has_focus() {
        ui.painter().rect(rect, radius, bg_colour, egui::Stroke::default());
    } else if response.hovered() {
        ui.painter().rect_stroke(rect, radius, stroke);
    }

    if response.has_focus() {
        let val = if ctx.input().key_pressed(egui::Key::Num1) {
            Some(1)
        } else if ctx.input().key_pressed(egui::Key::Num2) {
            Some(2)
        } else if ctx.input().key_pressed(egui::Key::Num3) {
            Some(3)
        } else if ctx.input().key_pressed(egui::Key::Num4) {
            Some(4)
        } else if ctx.input().key_pressed(egui::Key::Num5) {
            Some(5)
        } else if ctx.input().key_pressed(egui::Key::Num6) {
            Some(6)
        } else if ctx.input().key_pressed(egui::Key::Num7) {
            Some(7)
        } else if ctx.input().key_pressed(egui::Key::Num8) {
            Some(8)
        } else if ctx.input().key_pressed(egui::Key::Num9) {
            Some(9)
        } else {
            None
        };

        if let Some(val) = val {
            if let Err(e) = solver.add_value_to_board(row, col, val) {
                *error_msg = e.to_string();
            }
        }
    }

    response
}
