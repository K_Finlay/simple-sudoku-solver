pub struct Resources {
    pub icon_mode: egui_extras::RetainedImage,
    pub icon_info: egui_extras::RetainedImage,
}

impl Default for Resources {
    fn default() -> Self {
        let icon_mode =
            egui_extras::RetainedImage::from_image_bytes("icon_mode", include_bytes!("../resources/icon-mode.png"))
                .unwrap();

        let icon_info =
            egui_extras::RetainedImage::from_image_bytes("icon_info", include_bytes!("../resources/icon-info.png"))
                .unwrap();

        Self { icon_mode, icon_info }
    }
}
