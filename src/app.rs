use simple_sudoku_solver_engine::Solver;

use crate::{resources::Resources, sudoku_board, Config};

pub struct MyApp {
    solver:    Solver,
    is_solved: bool,
    error_msg: String,

    pub config: Config,
    resources:  Resources,
}

impl Default for MyApp {
    fn default() -> Self {
        Self {
            solver:    Solver::default(),
            is_solved: false,
            error_msg: String::new(),

            config:    Config::load(),
            resources: Resources::default(),
        }
    }
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        self.draw_top_panel(ctx);
        self.draw_main_content(ctx);
    }

    fn on_exit(&mut self, _gl: Option<&eframe::glow::Context>) {
        self.config.save();
    }
}

impl MyApp {
    /// Draws a top panel container the header label and settings buttons.
    #[inline]
    fn draw_top_panel(&mut self, ctx: &egui::Context) {
        egui::TopBottomPanel::top("top_panel")
            .resizable(false)
            .min_height(45.0)
            .show(ctx, |ui| {
                ui.horizontal_centered(|ui| {
                    // App title
                    ui.with_layout(
                        egui::Layout::centered_and_justified(egui::Direction::LeftToRight),
                        |ui| {
                            ui.heading("Simple Sudoku Solver");
                        },
                    );

                    ui.with_layout(egui::Layout::right_to_left(egui::Align::Center), |ui| {
                        ui.add_space(6.0);

                        // Light / Dark toggle
                        if ui
                            .add(
                                egui::ImageButton::new(
                                    self.resources.icon_mode.texture_id(ctx),
                                    self.resources.icon_mode.size_vec2(),
                                )
                                .tint(ui.visuals().noninteractive().fg_stroke.color),
                            )
                            .clicked()
                        {
                            if ui.visuals().dark_mode {
                                ctx.set_visuals(egui::style::Visuals::light());
                                self.config.dark_mode = false;
                            } else {
                                ctx.set_visuals(egui::style::Visuals::dark());
                                self.config.dark_mode = true;
                            }
                        }
                    });
                });
            });
    }

    /// Draw the main panel containing the Sudoku board and solve buttons.
    #[inline]
    fn draw_main_content(&mut self, ctx: &egui::Context) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.add_space(5.0);
            ui.label(
                egui::RichText::new(&self.error_msg)
                    .font(egui::FontId::proportional(16.0))
                    .color(egui::Color32::RED)
                    .strong(),
            );

            ui.horizontal_centered(|ui| {
                // Sudoku board
                ui.with_layout(egui::Layout::bottom_up(egui::Align::Min), |ui| {
                    ui.add(sudoku_board(ctx, &mut self.solver, &mut self.error_msg));
                });

                // Right side buttons
                ui.with_layout(egui::Layout::bottom_up(egui::Align::Center), |ui| {
                    // Solve button
                    if ui
                        .add_enabled_ui(!self.is_solved, |ui| {
                            ui.add_sized(egui::vec2(ui.available_width(), 50.0), egui::Button::new("Solve"))
                        })
                        .inner
                        .on_hover_text_at_pointer("Run the Sudoku solver")
                        .clicked()
                    {
                        self.error_msg.clear();
                        if let Err(e) = self.solver.solve() {
                            self.error_msg = e.to_string();
                        }
                        self.is_solved = true;
                    }

                    ui.add_space(5.0);

                    // Reset button
                    if ui
                        .add_sized(egui::vec2(ui.available_width(), 50.0), egui::Button::new("Reset"))
                        .on_hover_text_at_pointer("Reset the Sudoku board")
                        .clicked()
                    {
                        self.error_msg.clear();
                        self.solver = Solver::default();
                        self.is_solved = false;
                    }
                });
            });
        });
    }
}
