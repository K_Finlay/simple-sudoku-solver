//--------------------------------------------------------------------------------------------------
//-- TYPES -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::fmt::Debug;

use rand::prelude::SliceRandom;

/// Max iterations before failing to solve the puzzle
const MAX_ITERATIONS: usize = 5000;

/// A 2D array of tiles representing the Sudoku board
pub type Board = [[Tile; 9]; 9];

//--------------------------------------------------------------------------------------------------
//-- ENUMS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

/// All possible error types returned by the solver.
#[derive(thiserror::Error, Debug)]
pub enum ErrorTypes {

    /// Returned when attempting to set a tile to a value that already exists
    /// within the same row, column, or cell.
    #[error("Tile cannot have a value of {0}")]
    ImpossibleSetValue(u8),

    /// Returned when the solver reaches `MAX_ITERATIONS`.
    ///
    /// This is normally an indicator that the input is invalid or unsolvable.
    #[error("Failed to solve after {0} iterations")]
    Unsolvable(usize),
}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Clone, Debug, Default, Copy, Eq, PartialEq)]
pub struct Row(pub u8);

#[derive(Clone, Debug, Default, Copy, Eq, PartialEq)]
pub struct Col(pub u8);

#[derive(Clone, Debug, Default, Copy, Eq, PartialEq)]
pub struct Cell(pub u8);

//--------------------------------------------------------------------------------------------------

/// The sudoku solver.
pub struct Solver {
    /// Board used for the solver input.
    pub board: Board,
}

impl Default for Solver {
    fn default() -> Self {
        let mut board = Board::default();

        for index_row in 0..9 {
            for index_col in 0..9 {
                let (row, col) = (Row(index_row), Col(index_col));
                board[index_row as usize][index_col as usize] = Tile::new(row, col, Self::get_cell(row, col));
            }
        }

        Self { board }
    }
}

impl Solver {
    /// Create a new `Solver` from an array of inputs.
    ///
    /// # Arguments
    ///
    /// * `input`: An array of tuples which contain the row, column, and value.
    ///
    /// # Examples
    ///
    /// ```
    /// # use simple_sudoku_solver_engine::Solver;
    /// let input = vec![
    ///     // (Row, Column, Value)
    ///     (1, 5, 9),
    ///     (3, 2, 1),
    ///     (9, 7, 4)
    /// ];
    ///
    /// let solver = Solver::from_array(&input);
    /// ```
    #[must_use]
    pub fn from_array(input: &[(u8, u8, u8)]) -> Self {
        let mut solver = Self::default();

        for (row, col, val) in input {
            solver.add_value_to_board(Row(*row), Col(*col), *val).unwrap();
        }

        solver
    }

    /// Runs the Sudoku solver.
    pub fn solve(&mut self) -> Result<(), ErrorTypes> {
        let board_clone = self.board.clone();
        let mut iterations = 0;

        while self.board.iter().flatten().any(|tile| tile.current_value.is_none()) {
            if iterations > MAX_ITERATIONS {
                return Err(ErrorTypes::Unsolvable(MAX_ITERATIONS));
            }

            // Get the tile with the lowest entropy
            let tile = self
                .board
                .iter_mut()
                .flatten()
                .filter(|tile| tile.current_value.is_none())
                .min_by(|t1, t2| t1.possible_values.len().cmp(&t2.possible_values.len()))
                .unwrap();

            // Choose a random value from the possible values and return it.
            // If the vec is empty, start the solving process again as it cannot be completed
            // in this pass.
            let value = if let Some(value) = tile.possible_values.choose(&mut rand::thread_rng()) {
                *value
            } else {
                self.board = board_clone.clone();
                iterations += 1;
                continue;
            };

            // Set the new value for the tile, and update the possible values of all adjacent tiles.
            let (row, col, cell) = (tile.row, tile.col, tile.cell);
            tile.set_value(value)?;
            self.update_adjacent_tiles(row, col, cell, value, false);
        }

        Ok(())
    }

    /// Sets the value of a tile on the board, updating all adjacent tiles.
    pub fn add_value_to_board(&mut self, row: Row, col: Col, value: u8) -> Result<(), ErrorTypes> {
        let tile = &mut self.board[row.0 as usize - 1][col.0 as usize - 1];
        let old_value = tile.set_value(value)?;

        let (row, col, cell) = { (tile.row, tile.col, tile.cell) };
        if old_value.is_some() {
            self.update_adjacent_tiles(row, col, cell, old_value.unwrap(), true);
        }

        self.update_adjacent_tiles(row, col, cell, value, false);

        Ok(())
    }

    /// Clears the value of a tile on the board, updating all adjacent tiles.
    pub fn remove_value_from_board(&mut self, row: Row, col: Col) -> Result<(), ErrorTypes> {
        let tile = &mut self.board[row.0 as usize - 1][col.0 as usize - 1];
        let value = tile.clear_value().unwrap();
        
        let (row, col, cell) = { (tile.row, tile.col, tile.cell) };
        self.update_adjacent_tiles(row, col, cell, value, true);
        
        Ok (())
    }

    /// Update the possible values of all tiles in the given row, col, and cell.
    fn update_adjacent_tiles(&mut self, row: Row, col: Col, cell: Cell, value: u8, value_removed: bool) {
        let board = &mut self.board;

        let update_tile = |tile: &mut Tile| {
            if value_removed {
                tile.add_possible_value(value);
            } else {
                tile.remove_possible_value(value);
            }
        };
        
        // Update current row.
        board
            .iter_mut()
            .flatten()
            .filter(|tile| tile.row == row)
            .for_each(|tile| {
                update_tile(tile);
            });

        // Update current column.
        board
            .iter_mut()
            .flatten()
            .filter(|tile| tile.col == col)
            .for_each(|tile| {
                update_tile(tile);
            });

        // Update current cell.
        board
            .iter_mut()
            .flatten()
            .filter(|tile| tile.cell == cell)
            .for_each(|tile| {
                update_tile(tile);
            });
    }

    /// Returns the cell number at the given coordinates.
    const fn get_cell(row: Row, col: Col) -> Cell {
        let row_num = match row.0 {
            0..=2 => 0,
            3..=5 => 3,
            _ => 6,
        };

        let col_num = match col.0 {
            0..=2 => 1,
            3..=5 => 2,
            _ => 3,
        };

        Cell(row_num + col_num)
    }
}

//--------------------------------------------------------------------------------------------------

/// Represents a single tile on a sudoku board.
#[derive(Debug, Default, Clone)]
pub struct Tile {
    pub current_value:          Option<u8>,
    pub(crate) possible_values: Vec<u8>,

    row:  Row,
    col:  Col,
    cell: Cell,
}

impl Tile {
    fn new(row: Row, col: Col, cell: Cell) -> Self {
        Self {
            current_value: None,
            possible_values: vec![1, 2, 3, 4, 5, 6, 7, 8, 9],
            row,
            col,
            cell,
        }
    }

    /// Sets the Tile's current value.
    ///
    /// Returns an error of the value could not be set.
    fn set_value(&mut self, value: u8) -> Result<Option<u8>, ErrorTypes> {
        if !self.possible_values.contains(&value) {
            return Err(ErrorTypes::ImpossibleSetValue(value));
        }

        let old_value = self.clear_value();

        self.remove_possible_value(value);
        self.current_value = Some(value);

        Ok(old_value)
    }

    /// Clears the Tile's current value.
    fn clear_value(&mut self) -> Option<u8> {
        if self.current_value.is_none() {
            return None;
        }

        let value = self.current_value.take();
        self.add_possible_value(value.unwrap());

        value
    }

    /// Removes `value` from the possible values list.
    fn remove_possible_value(&mut self, value: u8) {
        if let Some(index) = self.possible_values.iter().position(|v| *v == value) {
            self.possible_values.remove(index);
        }
    }

    /// Adds `value` to the possible values list.
    fn add_possible_value(&mut self, value: u8) {
        if !self.possible_values.contains(&value) {
            self.possible_values.push(value);
        }
    }
}
